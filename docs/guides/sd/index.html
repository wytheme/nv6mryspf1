<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/_assets/site.css" rel="stylesheet">
    <link href="/_assets/favicon.png" rel="icon" type="image/png">
    <title>CS107E Working with SD cards</title>
  </head>

<body>
  <!-- Include this HTML partial to set up navbar -->

<nav class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
    <!-- hamburger -->
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>

  <img class="navbar-brand navbar-brand-image" src="/_assets/berry.png" >
  <a class="navbar-brand" href="/">CS107e Winter 2020</a>
  </div>

  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/">Home</a></li>
      <li><a href="/assignments">Assignments</a></li>
      <li><a href="/labs">Labs</a></li>
      <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Resources
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/about/">About the course</a></li>
            <li><a href="/policies/">Policies</a></li>
            <li><a href="/schedule/">Schedule</a></li>
            <li><a href="/guides/">Guides</a></li>
            <li><a href="/project_gallery/">Project gallery</a></li>
            <li><a href="/demos/">External demos</a></li>
            <li><a href="/resources/">External resources</a></li>
          </ul>
        </li>
      <li><a href="https://github.com/cs107e/cs107e.github.io">Repository</a></li>
    </ul>
  </div>
</nav>


  <div class="container" style="max-width:55em;">
    <h1 class="title">Working with SD cards</h1><hr>
    
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" id="main_for_toc">
    <p><em>Written by Pat Hanrahan</em></p>

<p>Your Raspberry Pi kit contains a microSDHC card. A secure digital (SD) card
stores data in non-volatile memory. The HC in SDHC stands for high
capacity. A micro-SD is shown below on the right. The adapter “jacket” on the left allows the small card to be used in a full-size SD card reader.</p>

<p><img src="../images/sd.kingston.jpg" alt="micro card with adapter" /></p>

<p>When a Raspberry Pi boots, it accesses the card to read the file named <code class="highlighter-rouge">kernel.img</code> and executes that program.</p>

<ol>
  <li>Prepare the card with the proper files by mounting it on your laptop and copying the necessary files onto it.</li>
  <li>Eject the card from your laptop and insert it into Raspberry Pi.</li>
  <li>When the Pi boots, it runs the program on the card.</li>
  <li>To change the program, repeat the process and copy the updated file. A much less tedious long-term solution is running a <a href="/guides/bootloader">bootloader</a>.</li>
</ol>

<h4 id="mount-sd-card-on-laptop">Mount SD card on laptop</h4>
<p>To copy a program to the SD card, you need to mount it on your laptop. If your laptop does not have a built-in SD card reader, you will need an external reader (we have a few to loan in lab). To mount the card, first insert the micro-SD into the adapter, and then insert the adapter into the SD card slot.</p>

<p><img src="../images/sd.mac.jpg" alt="insert sd card into slot" /></p>

<p>The SD card mounts automatically and will show up in your finder along with other mounted file systems.</p>

<p>Verify that the card is mounted.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ ls /Volumes
Macintosh HD    NO NAME
</code></pre></div></div>

<p>By default, the volume is named <code class="highlighter-rouge">NO NAME</code>.</p>

<h4 id="copy-files-onto-card">Copy files onto card</h4>

<p>The necessary files are stored in the firmware directory of our <a href="https://github.com/cs107e/cs107e.github.io/tree/master/firmware">courseware
repository</a>. There are 4 files.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ ls firmware
blink-actled.bin   bootcode.bin     bootloader.bin  start.elf     
</code></pre></div></div>

<p><code class="highlighter-rouge">bootcode.bin</code> and <code class="highlighter-rouge">start.elf</code> are the start files necessary for the GPU. The Pi boot sequence next looks for a file called <code class="highlighter-rouge">kernel.img</code>, the program to run. Normally <code class="highlighter-rouge">kernel.img</code> is the linux kernel. In this course, we will replace the linux kernel with our
own program.</p>

<p>Our firmware folder has two programs, <code class="highlighter-rouge">blink-actled.bin</code> and
<code class="highlighter-rouge">bootloader.bin</code>.  As a first step, copy <code class="highlighter-rouge">blink-actled.bin</code>
to <code class="highlighter-rouge">kernel.img</code>. This program
blinks the on-board activity (ACT) LED. This is a
good way to test whether your Pi is working.</p>

<p>Later, after you’ve verified the blink program, you can
replace <code class="highlighter-rouge">kernel.img</code> with <code class="highlighter-rouge">bootloader.bin</code> so that Pi will boot and run a boot
loader that can receive uploaded programs.</p>

<p>Copy the firmware files onto your SD card and eject it. On a Mac, click on the Eject icon next to the volume name in the Finder.</p>

<h4 id="insert-card-into-raspberry-pi">Insert card into Raspberry Pi</h4>
<p>Remove the micro-SD from its adapter and insert it into the slot on the underside of the Raspberry Pi circuit board. There is a small spring that holds the card in place. As you push it in, you will feel the mechanism grab onto the card.</p>

<p><img src="../images/sd.pi.jpg" alt="micro SD inserted on Pi" /></p>

<!-- This partial will open warning/danger callout box  -->

<div style="background-color:#ffcccc; color:#993333; border-left: 5px solid #993333;margin: 5px 25px; padding: 5px;">

  <p><strong>Take care!</strong> To eject the micro-SD from the Pi’s card slot, gently push the card in and allow it to spring back out. If you try to pull out the card by force, you can break the mechanism and potentially destroy your Pi.</p>
</div>

<h3 id="troubleshooting">Troubleshooting</h3>

<ol>
  <li>The SD cards we are using are formatted with a FAT32 filesystem. If this
file system is corrupted, you will need to reformat the file system.
Consult the internet for how to do this.</li>
  <li>The Raspberry Pi can become wedged if <code class="highlighter-rouge">kernel.img</code> has bugs or
becomes corrupted. You can always recover to a known state by copying the
original firmware to the SD card.</li>
</ol>


  </div>
  <div class="toc-column col-lg-2 col-md-2 col-sm-2 hidden-xs">
    <div id="toc" class="toc" data-spy="affix" data-offset-top="0"></div>
  </div> 
</div>

  <script src="/_assets/tocbot.min.js"></script>
  <link rel="stylesheet" href="/_assets/tocbot.css">

  <script>
    tocbot.init({
      // Where to render the table of contents.
      tocSelector: '#toc',
      // Where to grab the headings to build the table of contents.
      contentSelector: '#main_for_toc',
      // Which headings to grab inside of the contentSelector element.
      headingSelector: 'h2, h3, h4',
    });
  </script>



  </div>
  <div class="footer navbar-default navbar-static-bottom">
    <p style="font-size: 65%; color:green; text-align:center;">
      <i>CS107e Winter 2020 &middot; Site generated 2020-04-19 20:35</i>
  </p>
</div> 
</body>

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</html>