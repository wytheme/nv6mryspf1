#ifndef BASIC_H
#define BASIC_H

#define true 1
#define false 0

#include <stdint.h>

typedef unsigned int uint;
typedef uint32_t u32;
typedef uint8_t u8;
typedef int bool;

#endif
