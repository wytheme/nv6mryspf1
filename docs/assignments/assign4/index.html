<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/_assets/site.css" rel="stylesheet">
    <link href="/_assets/favicon.png" rel="icon" type="image/png">
    <title>CS107E Assignment 4: Backtrace and Malloc</title>
  </head>

<body>
  <!-- Include this HTML partial to set up navbar -->

<nav class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
    <!-- hamburger -->
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>

  <img class="navbar-brand navbar-brand-image" src="/_assets/berry.png" >
  <a class="navbar-brand" href="/">CS107e Winter 2020</a>
  </div>

  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/">Home</a></li>
      <li><a href="/assignments">Assignments</a></li>
      <li><a href="/labs">Labs</a></li>
      <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Resources
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/about/">About the course</a></li>
            <li><a href="/policies/">Policies</a></li>
            <li><a href="/schedule/">Schedule</a></li>
            <li><a href="/guides/">Guides</a></li>
            <li><a href="/project_gallery/">Project gallery</a></li>
            <li><a href="/demos/">External demos</a></li>
            <li><a href="/resources/">External resources</a></li>
          </ul>
        </li>
      <li><a href="https://github.com/cs107e/cs107e.github.io">Repository</a></li>
    </ul>
  </div>
</nav>


  <div class="container" style="max-width:55em;">
    <h1 class="title">Assignment 4: Backtrace and Malloc</h1><hr>
    
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" id="main_for_toc">
    
<p><em>Written by Julie Zelenski</em></p>

<!-- If duedate set in front_matter, use that, otherwise calculate based on n*weeks + assign1 due -->

<p><b>Due: Tuesday, February 11 at  6:00 pm</b></p>

<h2 id="goals">Goals</h2>
<p>For this week’s assignment, you are again acting in the role of
library implementer to add a debugging aid (<code class="highlighter-rouge">backtrace.c</code>)
and a heap allocator (<code class="highlighter-rouge">malloc.c</code>) to your growing
collection of foundational routines.</p>

<p>Global and local variables have been sufficient for our needs thus far, but adding support for dynamic allocation will enable more sophisticated applications. Dynamic allocation offers the programmer explicit and 
direct control over sizing and lifetime of storage. With this
power comes with the responsibility to properly allocate and
deallocate that memory. Using dynamic memory correctly and safely is a
challenge for the client; implementing a heap allocator correctly
and efficiently is a challenge for the implementer.</p>

<p>After you finish this assignment, you will:</p>

<ul>
  <li>
    <p>be able to dissect the runtime stack in an executing program,</p>
  </li>
  <li>
    <p>further understand C data structures and memory layout,</p>
  </li>
  <li>
    <p>have leveled up your skills wrangling pointers in C, and</p>
  </li>
  <li>
    <p>appreciate the complexity and tradeoffs in implementing a heap allocator.</p>
  </li>
</ul>

<p>Each new assignment also provides much opportunity for you to reap the benefits of the hard work you invested in previous weeks. For the rest of quarter, let the refrain “my <code class="highlighter-rouge">printf</code> rocks!” ring out loud and proud.</p>

<h2 id="get-starter-files">Get starter files</h2>
<p>Change to the <code class="highlighter-rouge">cs107e.github.io</code> repository in your <code class="highlighter-rouge">cs107e_home</code> and do a <code class="highlighter-rouge">git pull</code> to ensure your courseware files are up to date.</p>

<p>To get the assignment starter code, change to your local repository, fetch any changes from the remote and switch to the assignment basic branch:</p>
<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ cd ~/cs107e_home/assignments
$ git fetch origin
$ git checkout assign4-basic
</code></pre></div></div>

<p>Verify that the files <code class="highlighter-rouge">gpio.c</code>, <code class="highlighter-rouge">timer.c</code>, <code class="highlighter-rouge">strings.c</code>, and <code class="highlighter-rouge">printf.c</code> in your assign4-basic branch are up-to-date versions of your modules.  The <code class="highlighter-rouge">git merge</code> command can be used to incorporate changes from another branch. For example, if you fixed issues in <code class="highlighter-rouge">gpio.c</code> or <code class="highlighter-rouge">timer.c</code> for an assignment 2 resubmit, use <code class="highlighter-rouge">git merge assign2-basic</code> to merge those changes into your current branch.  If you continued work on <code class="highlighter-rouge">assign3-basic</code>
after your assign4-basic branch was created, use <code class="highlighter-rouge">git merge assign3-basic</code> to merge those latest updates.</p>

<p>You have the option of building on your own code as you move forward or switching out your modules for our library versions. Edit the setting <code class="highlighter-rouge">MY_MODULES</code> in the Makefile to control which modules of yours are used and which of ours.</p>

<p>The starter project contains the files <code class="highlighter-rouge">backtrace.c</code> and
<code class="highlighter-rouge">malloc.c</code>, the sample application program <code class="highlighter-rouge">apps/heapclient.c</code>, and the test program <code class="highlighter-rouge">tests/test_backtrace_malloc.c</code>.</p>

<p>You will edit <code class="highlighter-rouge">backtrace.c</code> and <code class="highlighter-rouge">malloc.c</code> to implement the required functions. You will also add many tests to <code class="highlighter-rouge">tests/test_backtrace_malloc.c</code>.  The program <code class="highlighter-rouge">apps/heapclient.c</code> is used unchanged as a sample application.</p>

<p>The <code class="highlighter-rouge">make install</code> target of the Makefile builds and runs the sample application <code class="highlighter-rouge">apps/heapclient.bin</code>. The <code class="highlighter-rouge">make test</code> target builds and runs the test program <code class="highlighter-rouge">tests/test_backtrace_malloc.bin</code>. With no argument, 
<code class="highlighter-rouge">make</code> will build both, but not run. You can use the debugger in simulator mode on the corresponding ELF file, e.g. <code class="highlighter-rouge">arm-none-eabi-gdb tests/test_backtrace_malloc.elf</code>.</p>

<h2 id="basic-section">Basic section</h2>

<h3 id="backtrace-module">Backtrace module</h3>
<p>The <code class="highlighter-rouge">backtrace</code> module provides functionality to gather and print a stack backtrace. A backtrace is a listing of the function call stack beginning at the currently executing function, showing the sequence of calls that led to this function, and ending at <code class="highlighter-rouge">_cstart</code>.</p>

<p>Consider a program whose <code class="highlighter-rouge">main</code> function calls <code class="highlighter-rouge">tokenize</code>, which then calls <code class="highlighter-rouge">strndup</code>. A backtrace initiated during <code class="highlighter-rouge">strndup</code> could look something like this:</p>
<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>#0 0x8090 at strndup+40
#1 0x814c at tokenize+116
#3 0x8354 at main+24
#4 0x83c0 at _cstart+48
</code></pre></div></div>

<p>The functions you are to implement in <code class="highlighter-rouge">backtrace.c</code> are:</p>

<ul>
  <li><code class="highlighter-rouge">int backtrace(frame_t f[], int max_frames)</code></li>
  <li><code class="highlighter-rouge">void print_frames(frame_t f[], int n)</code></li>
  <li><code class="highlighter-rouge">void print_backtrace(void)</code></li>
  <li><code class="highlighter-rouge">const char *name_of(uintptr_t fn_start_addr)</code></li>
</ul>

<p>Review our <a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/backtrace.h">backtrace.h</a> for module documentation.
The two <code class="highlighter-rouge">print_</code> functions are given to you pre-written. You are to implement the <code class="highlighter-rouge">name_of</code> and <code class="highlighter-rouge">backtrace</code> functions. <code class="highlighter-rouge">name_of</code> finds the name for a given function and  <code class="highlighter-rouge">backtrace</code> fills an array of stack frame information. These two functions are small but mighty. Completing them will demonstrate your understanding of the stack layout and show off your finest pointer skills.</p>

<p>Before writing any code for the backtrace module, we recommend that you first spend time poking around in gdb and exploring the runtime state of the program, examining stack memory, stepping through function calls, and manually accessing the stack frame data, and so on. Exercise 4 of <a href="/labs/lab4/#4-stack">Lab 4</a> may be worth a second look.</p>

<h4 id="an-example-backtrace">An example backtrace</h4>

<p>The <code class="highlighter-rouge">backtrace</code> function fills an array of structs, one <code class="highlighter-rouge">frame_t</code> for each function on the stack. Read
<a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/backtrace.h">backtrace.h</a> 
to see the type definition of the struct <code class="highlighter-rouge">frame_t</code>, including the comments which explain the intent of the fields <code class="highlighter-rouge">resume_addr</code> and <code class="highlighter-rouge">resume_offset</code>.</p>

<p>The <code class="highlighter-rouge">backtrace</code> function starts at the innermost stack frame and traces to the outermost, harvesting the information for each stack frame and storing into a <code class="highlighter-rouge">frame_t</code>.</p>

<p>The current value of the <code class="highlighter-rouge">fp</code> register is the “anchor” you need to start examining the stack. You must drop down to assembly to access a register. The GCC <code class="highlighter-rouge">__asm__</code> keyword allows you to mix assembly in with 
your C code. The code sample below demonstrates using embedded assembly to store the value of the <code class="highlighter-rouge">fp</code> register into a variable named <code class="highlighter-rouge">cur_fp</code>:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>   void *cur_fp;
   __asm__("mov %0, fp" : "=r" (cur_fp));
</code></pre></div></div>

<p>(If you’re 
   curious to learn more, check out the manual: <a href="https://gcc.gnu.org/onlinedocs/gcc/Using-Assembly-Language-with-C.html">gcc support for inline assembly</a>.)</p>

<p>We are using the <code class="highlighter-rouge">code/simple</code> program from lab3/lab4 as our example below. Have the Lab 4
   <a href="/labs/lab4/images/stack_abs.html">memory diagram for simple</a> handy when working through the following thought questions.</p>

<ul>
  <li>
    <p>The executing function is <code class="highlighter-rouge">abs()</code>. What is the value of the <code class="highlighter-rouge">fp</code> register? Relative to the <code class="highlighter-rouge">fp</code>, where in the stack can you read the <code class="highlighter-rouge">resume_addr</code> for the caller of <code class="highlighter-rouge">abs()</code>?</p>
  </li>
  <li>
    <p>How can you work from the <code class="highlighter-rouge">fp</code> for <code class="highlighter-rouge">abs()</code> to access the saved <code class="highlighter-rouge">fp</code> of its caller, the <code class="highlighter-rouge">diff</code> function?  How does the saved <code class="highlighter-rouge">fp</code> grant you access to the stack frame for <code class="highlighter-rouge">diff</code>? How do you then go from <code class="highlighter-rouge">diff</code>’s frame to the frame of its caller, the <code class="highlighter-rouge">main</code> function? What is the indication that you have hit the outermost stack frame? This is where backtrace stops.</p>
  </li>
  <li>
    <p>The <code class="highlighter-rouge">resume_offset</code> is calculated by subtracting the address of the first instruction of the caller from the resume address in the caller. What information in the stack frame for <code class="highlighter-rouge">diff</code> helps you locate its first instruction? (Hint: may help to re-visit question 4 on the 
<a href="/labs/lab4/checkin/">lab4 check-in</a> 
for some foreshadowing about this calculation.)</p>
  </li>
</ul>

<p>The final task in harvesting the backtrace is to associate the name of the function with each stack frame. Symbol names are usually not present in a binary file as functions are referred to by address, not name. However, there is a compiler option to retain function names in the binary to support developers (such as yourself)
writing debugging tools. The assignment Makefile has the flag <code class="highlighter-rouge">-mpoke-function-name</code> enabled in <code class="highlighter-rouge">CFLAGS</code>. This option tells
gcc to store a function’s name with the compiled instructions, embedded in the memory preceding the function’s first instruction.</p>

<!-- This partial will open warning/danger callout box  -->

<div style="background-color:#ffcccc; color:#993333; border-left: 5px solid #993333;margin: 5px 25px; padding: 5px;">

  <p>The documentation below is taken from the gcc manual, but we have corrected a few mis-identified details. Please use this version rather than refer back to the original.</p>

</div>

<blockquote>
  <p><code class="highlighter-rouge">-mpoke-function-name</code></p>

  <p>Write the name of each function into the text section, directly
preceding the function prologue. The generated code is similar to
this:</p>

  <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>t0
    .ascii "arm_poke_function_name", 0
    .align
t1
    .word 0xff000000 + (t1 - t0)
arm_poke_function_name
    mov     r12, sp
    push    {fp, r12, lr, pc}
    sub     fp, r12, #4
</code></pre></div>  </div>

  <p>When performing a stack backtrace, code can inspect the word value at the
location immediately preceding the first instruction. If that value 
has the most significant 8 bits set, then we know that there is a
function name embedded preceding value and the name has
length <code class="highlighter-rouge">(value &amp; ~0xff000000)</code>.</p>
</blockquote>

<p>The <code class="highlighter-rouge">name_of</code> function is given the address of a function’s first instruction and finds the embedded function name, if available. If the word preceding a function’s first instruction in memory has <code class="highlighter-rouge">0xff</code> in its most significant byte, this indicates that the function has its name embedded. The name’s length is stored in the lower three bytes of the word. Use that length to back up in memory to the start of the string. Neat! If you don’t find the magic <code class="highlighter-rouge">0xff</code> in the right place, this indicates the function name is not available, in which case, <code class="highlighter-rouge">name_of</code> should return <code class="highlighter-rouge">"???"</code>.</p>

<p>The embedded function name is null-terminated and the terminator is
counted in the length. The <code class="highlighter-rouge">.align</code> directive rounds up the length to a 
multiple of 4; additional null characters are used as padding. 
A function named <code class="highlighter-rouge">binky</code> will have its name embedded 
as <code class="highlighter-rouge">binky\0\0\0</code> and length of 8. Given that the name is
stored with its null terminator, <code class="highlighter-rouge">name_of</code> can access the string in-place and simply refer to it by address without making a copy of of the string.</p>

<p>If our previous example program is re-compiled with the <code class="highlighter-rouge">-mpoke-function-name</code> flag, the text section will contain the function names embedded alongside the instructions, (see <a href="images/diagram_with_poke.html">memory diagram with embedded function names</a>). Using this diagram as a guide, work out by what steps you could access a function’s name if starting from the address for the function’s first instruction. This is what needs to happen in <code class="highlighter-rouge">name_of</code>.</p>

<p>Put this all together and you have a module capable of harvesting a backtrace for the current stack, including function names. It requires surprisingly little code, but it’s dense and heavy on the pointer arithmetic and typecasts. Carefully map out your approach, take it slow, and use gdb as you go to examine memory and print expressions to confirm your steps. Validating your backtrace against <code class="highlighter-rouge">gdb</code>’s <code class="highlighter-rouge">backtrace</code> command can also be a useful debugging technique.</p>

<p>The stack layout follows a rigid convention, so once you have code to correctly backtrace a stack, it is likely to work equally well for all other stacks without special cases. Confirm with some simple tests and you’re ready to move on.</p>

<h3 id="malloc-module">Malloc module</h3>

<p>The job of the heap allocator is to service allocation requests and manage the pool of available space.</p>

<p>The functions you are to implement in <code class="highlighter-rouge">malloc.c</code> are:</p>

<ul>
  <li><code class="highlighter-rouge">void *malloc(size_t nbytes)</code></li>
  <li><code class="highlighter-rouge">void free(void *ptr)</code></li>
  <li><code class="highlighter-rouge">void *realloc(void *ptr, size_t new_size)</code></li>
</ul>

<p>The module documentation in <a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/malloc.h">malloc.h</a> describes the functions. They operate in the same spirit as the same-named allocation functions of the standard C library.</p>

<p>The essential requirement for any heap allocator is that it correctly service any combination of well-formed requests.  It is also desirable that it be space-efficient (recycling freed memory, compactly storing blocks) and fast (quickly servicing requests). For the assignment, your primary goal will be to achieve correctness, with a lesser priority on efficiency.</p>

<h4 id="malloc-specification">Malloc specification</h4>

<p>Refer to the module documentation in <a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/malloc.h">malloc.h</a> for the operational behavior of each function. Other than the non-negotiable stipulation to correctly service all valid requests, there are very requirements to the specification itself.</p>

<p>All pointers returned by <code class="highlighter-rouge">malloc</code> are required be aligned to 8 bytes, the size of the largest primitive type on our system.</p>

<p>If <code class="highlighter-rouge">malloc</code> cannot fulfill an allocation request, it returns <code class="highlighter-rouge">NULL</code>.  Four valid, but oddball cases to consider are <code class="highlighter-rouge">malloc(0)</code>, <code class="highlighter-rouge">realloc(NULL, size)</code>,  <code class="highlighter-rouge">realloc(ptr, 0)</code> and <code class="highlighter-rouge">free(NULL)</code>.  The expectation for these edge cases is given in the malloc.h documentation.</p>

<p>If a client request is invalid, such as an attempt to <code class="highlighter-rouge">realloc</code> a
random pointer that was not obtained from <code class="highlighter-rouge">malloc</code> or <code class="highlighter-rouge">free</code>ing an already freed pointer, the behavior of the allocator is undefined. We will not test such invalid calls.</p>

<h4 id="order-of-attack">Order of attack</h4>
<p>Below we offer our suggestions for a step-by-step plan  of the tasks before you.</p>

<ol>
  <li>
    <p><strong>Study starter code.</strong></p>

    <p>Start by reviewing the given code for the “bump allocator” in the file <code class="highlighter-rouge">malloc.c</code>.  This bump allocator is simple and correct, but fairly naive. 
It serves as a reference point 
from which your own
heap allocator will be able to show substantial improvements.</p>

    <p>Make sure you understand what each line in the bump allocator is
doing. Your allocator will be significantly more complicated, so it is best to start from a solid foundation. Here are some self-test questions to work through to verify your understanding:</p>

    <ul>
      <li>Trace the given code for <code class="highlighter-rouge">sbrk</code>. You will use this code as-is and it is important that you understand exactly what this function does for you and how to use it. Where does the heap segment start in memory? Where does it end? How does it enlarge? How big can it grow? What is the result of attempting to grow beyond the available limit?</li>
      <li>Why does the code cast pointers to <code class="highlighter-rouge">char *</code> before doing pointer arithmetic?</li>
      <li>If you request a 5-byte allocation, how much space is actually reserved?</li>
      <li>Every allocation is required to be 8-byte aligned (i.e. payload must start on an address which is a multiple of 8). How does the bump allocator achieve this requirement?</li>
      <li>Why is the bump allocator unable to recycle a previous allocation that has been freed?</li>
      <li>Review the documentation for the <code class="highlighter-rouge">realloc</code> operation. Review the realloc code in the bump allocator and confirm your understanding of how the code accomplishes resizing the block. The bump allocator never performs an in-place realloc. What is meant by an “in-place realloc” and how does that compare to what the bump allocator does instead?</li>
    </ul>
  </li>
  <li>
    <p><strong>Re-work <code class="highlighter-rouge">malloc()</code> to add a block header to each block.</strong></p>

    <p>Your heap allocator will place a header on each block that tracks the block size and status (in-use or free).  Review these <a href="block_headers/">block header diagrams</a> to understand the basic layout of a heap using block headers.</p>

    <p>The bump allocator services a <code class="highlighter-rouge">malloc</code> request by extending from the break to place the new block at the previous heap end. Continue with the same strategy for now, but augment the operation to add a block header on the block.</p>

    <p>When adding the header, be sure to consider how it will affect payload alignment. Each payload must start on an address that is a multiple of 8. One convenient way to adhere to the alignment rule is
 to round up each payload to a size that is a multiple of 8, add an 8-byte header, and lay out blocks end to end starting from an address that is a multiple of 8. Our standard <code class="highlighter-rouge">memmap</code> aligns the <code class="highlighter-rouge">__bss_end__</code> symbol to an 8-byte boundary.</p>
  </li>
  <li>
    <p><strong>Implement <code class="highlighter-rouge">free()</code> to update status in block header.</strong></p>

    <p><code class="highlighter-rouge">free()</code> should access the block’s header and mark it as free. Accessing the neighboring memory at a negative offset via pointer subtraction works just like pointer addition – handy!</p>

    <p>This doesn’t yet recycle a free block, but marking it as free is the
 first step toward that.</p>
  </li>
  <li>
    <p><strong>Implement <code class="highlighter-rouge">heap_dump()</code> to output debugging information.</strong></p>

    <p>Before things get any more complex, implement a routine to give you visibility on what’s going on inside your heap.  The intention of <code class="highlighter-rouge">heap_dump()</code> is to print a visual dump of your heap contents that will help you when debugging your allocator.  Yay <code class="highlighter-rouge">printf</code>, where have you been all my life?!</p>

    <p>As a starting point, we suggest that <code class="highlighter-rouge">heap_dump</code> walk the heap and print each block. Starting from the first block, traverse from block to block until you reach the heap end. You can move from a block header to its forward neighbor by using pointer arithmetic to advance by the block size. The block headers are said to form an <em>implicit list</em>.</p>

    <p>For each block, print the payload address and size and status. You might also elect to print the payload data, and if so, we recommend abbreviating, perhaps displaying only the first 16 bytes. If you instead print out every single byte, the output can become overwhelming for larger heaps.</p>

    <p>Use the <code class="highlighter-rouge">trace_heap()</code> test in <code class="highlighter-rouge">tests/test_backtrace_malloc.c</code> to try out <code class="highlighter-rouge">heap_dump</code>. You can add calls to <code class="highlighter-rouge">heap_dump()</code> wherever desired to get a visual dump of your heap contents for debugging. If you inspect the dump and see that your heap is in a corrupted state, add calls to dump earlier in the sequence and try to pinpoint the exact operation where your heap went from good to bad. Having an insider view of what’s going on inside your heap is an invaluable aid for debugging.</p>

    <p>What you print in <code class="highlighter-rouge">heap_dump</code> will not be graded, this debugging function is yours to use as you see fit, but the more help it provides to you, the better off you will be.</p>
  </li>
  <li>
    <p><strong>Upgrade <code class="highlighter-rouge">malloc()</code> to search freed blocks and reuse/split.</strong></p>

    <p>Now change <code class="highlighter-rouge">malloc</code> to recycle a free block if possible. Walk the heap by traversing the implicit list of headers, and look for a free block of an appropriate size. A block that exactly matched the requested size would be ideal, but searching the entire heap to find that best fit block can be time-consuming. A quicker fix is to go with <em>first fit</em>, i.e. grab the first block found that is large enough. If this block satisfies the request with enough excess leftover to form a second block, split off that remainder into its own free block.</p>

    <p>If no recyclable block is found, then just extend the heap and place the new block at the end as before.</p>

    <p>Concoct a sequence of malloc/free calls that should permit reuse and confirm with <code class="highlighter-rouge">heap_dump</code> that your allocator is recycling freed memory appropriately.</p>
  </li>
  <li>
    <p><strong>Upgrade <code class="highlighter-rouge">free()</code> to coalesce adjacent free blocks.</strong></p>

    <p>Reusing single freed blocks is a good improvement, but there is further opportunity for recycling. What happens if the client repeatedly allocates 8-byte blocks until the heap is full, frees all of them, then tries to allocate a 32-byte block? The heap contains way more than 32 bytes of free space, but it has been fragmented it into individual 8-byte blocks.  To service a larger request, we’ll need to join those smaller blocks.</p>

    <p>Modify <code class="highlighter-rouge">free()</code> so that when freeing a block, it checks if the block’s forward neighbor is also free, and if so, it will <em>coalesce</em> the two
 blocks into one combined free block. It then considers the forward neighbor of
 that bigger block to see if it’s free and can also be coalesced. Continue
 forward-coalescing until no further coalescing is possible.</p>

    <p>Confirm forward-coalesce with test sequences and use of <code class="highlighter-rouge">heap_dump</code>.</p>
  </li>
  <li>
    <p><strong>Upgrade <code class="highlighter-rouge">realloc()</code> to operate in-place when possible.</strong></p>

    <p>The final feature is to make a smarter <code class="highlighter-rouge">realloc</code>. The version of <code class="highlighter-rouge">realloc</code> given in the starter code always moves the payload data from the original block to a new block. If possible, we’d like to avoid that (expensive) copying and leave the payload data where it is. If the neighboring space is free, <code class="highlighter-rouge">realloc</code> could instead subsume the forward neighbor(s) into the original block, thereby resizing “in-place”. Modify <code class="highlighter-rouge">realloc()</code> to seize this opportunity to resize the block in-place and only move the payload data when it must.</p>

    <p>Another round of tests and checking your heap contents with <code class="highlighter-rouge">heap_dump</code> and you’ve good to go!</p>
  </li>
</ol>

<p><strong>Congratulations!</strong> You have summited Heap Allocator Mountain ⛰ and earned your Pointer Wrangling Merit Badge 🏆 Time to celebrate with a nice cup of tea ☕️, a moment of mediation 🧘‍♀️, or a decadent nap in the sun ☀️.</p>

<p><a name="testing"></a></p>
<h2 id="testing">Testing</h2>

<p>The <code class="highlighter-rouge">apps/heapclient.c</code> program contains sample client code adapted from lab4 that uses <code class="highlighter-rouge">backtrace</code> and <code class="highlighter-rouge">malloc</code>. Use this program unchanged as a basic sanity test of your modules.</p>

<p>The <code class="highlighter-rouge">tests/test_backtrace_malloc.c</code> program is yours to fill with your comprehensive suite of test cases. The starter code provides some example tests to get you started. You will need to add many more tests to throughly exercise your heap allocator in a wide variety of situations. A portion of your assignment grade is awarded based on the quality and breadth of the test cases you submit.</p>

<p>In CS106B, you learned about a variety of dynamic data structures (linked lists, trees, hash tables) and used them in the implementation of classic abstract data types (vector, stack, queue, dictionary, set). Given your shiny new heap allocator, you can now bring all that wonderful data structure goodness to your Pi.  Porting some of that code would make excellent and rigorous test cases for your allocator and in the process, you would gain a library of useful ADTs – win-win!</p>

<h2 id="extension-mini-valgrind">Extension: Mini-Valgrind</h2>
<p>The extension for this assignment is to combine your backtrace and malloc modules to add a “Mini-Valgrind” set of memory debugging features to your heap allocator to report on memory errors and memory leaks.</p>

<p>What happens if a client allocates a block of memory and then mistakenly writes outside of the bounds?  The erroneous write not only stomps on the neighboring payload data, it is almost certain to destroy the neighboring block header. A memory error that corrupts this critical heap housekeeping will lead to many sad consequences.</p>

<p>What happens if a client allocates a block, but 
forgets to free it when done? This creates a <em>memory leak</em>,
where the block is parked there forever, unable to be recycled. A memory leak is much less devastating than a memory error, but can degrade performance over time and eventually cause issues.</p>

<p>Memory debugging tools such as <a href="http://valgrind.org">Valgrind</a> are invaluable weapons in the battle against difficult-to-find memory errors and leaks. For the extension, you are to implement a “Mini-Valgrind” that adds memory error and leak detection to your heap allocator.</p>

<h4 id="red-zones-and-leak-detection">Red zones and leak detection</h4>

<p>One technique for detecting memory over/underruns is to surround each payload with a pair of <em>red zones</em>. When servicing a malloc request, oversize the space by an extra 8 bytes. Place the actual payload in the middle with one 4-byte red zone before it and another 4-byte red zone after it. (Take care to keep the payload aligned on the 8-byte boundary). Fill the red zone with a distinctive value. Our implementation uses a repeated <code class="highlighter-rouge">0x7e</code>, though you are free to use <a href="https://en.wikipedia.org/wiki/Hexspeak">any distinctive non-offensive value you like</a>. When the client later frees that block, check the red zones and squawk loudly if the value has been perturbed.</p>

<p>In addition, you should tag each allocated block with the context it was allocated from by recording a “mini-backtrace”. Modify your block header structure to add an array of three <code class="highlighter-rouge">frame_t</code> and fill with a snapshot of the innermost three frames at the point this block was allocated.</p>

<p>Modify <code class="highlighter-rouge">free</code> to verify that the red zones are intact for the block currently being freed. If not, print an error message about the block in error along with its mini-backtrace that identifies where the block was allocated from.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>=============================================
 **********  Mini-Valgrind Alert  ********** 
=============================================
Attempt to free address 0x0000a978 that has damaged red zone(s): [457e7e7e] [7e7e7e7e]
Block of size 5 bytes, allocated by
#0 0x8d4c at malloc+192
#1 0x8650 at test_heap_redzones+52
#2 0x86bc at main+56
</code></pre></div></div>

<p>Format your error message as shown in the sample above. The values for the address, damaged red zone, block size, etc. will differ based on the test case, but you should otherwise exactly match the wording and format so as to be compatible with our automated checker.</p>

<p>Mini-Valgrind also tracks heap statistics, such as the count of calls to <code class="highlighter-rouge">malloc</code> and <code class="highlighter-rouge">free</code> and the aggregate total number of bytes requested. Implement the function <code class="highlighter-rouge">memory_report</code> that is intended to be used at program completion to print a summary of the total heap usage and list all remaining memory leaks. The mini-backtrace stored in the block header is used to identify the context for each leaked block.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>=============================================
         Mini-Valgrind Memory Report         
=============================================

malloc/free: 42 allocs, 40 frees, 5752 bytes allocated

8 bytes are lost, allocated by
#0 0x8d0c at malloc+196
#1 0x8674 at main+28
#2 0x86ec at _cstart+48

100 bytes are lost, allocated by
#0 0x8d0c at malloc+196
#1 0x868c at main+52
#2 0x86ec at _cstart+48

Lost 108 total bytes in 2 blocks. 
</code></pre></div></div>
<p>The format of your memory report should exactly match the wording and format of the sample report shown above.</p>

<p>As a final detail, work out where you can call <code class="highlighter-rouge">memory_report()</code> so that Mini-Valgrind can provide leak detection to any program, without modifying the application’s
<code class="highlighter-rouge">main</code> or other application files? Hint: think about how the green LED 
turns on when your test program runs to successful completion.</p>

<h2 id="submit">Submit</h2>
<p>The deliverables for <code class="highlighter-rouge">assign4-basic</code> are:</p>

<ul>
  <li>Implementation of the <code class="highlighter-rouge">backtrace.c</code> and <code class="highlighter-rouge">malloc.c</code> modules</li>
  <li>Your comprehensive tests for all backtrace and malloc functions in <code class="highlighter-rouge">tests/test_backtrace_malloc.c</code></li>
</ul>

<p>Submit the finished version of your assignment by making a git “pull request”. Make separate pull requests for your basic and extension submissions.</p>

<p>The automated checks make sure that we can run your C
code and test and grade it properly, including swapping your tests for
ours.</p>

<p>CI verifies that:</p>

<ul>
  <li>
    <p><code class="highlighter-rouge">apps/heapclient.c</code> is unchanged</p>
  </li>
  <li>
    <p><code class="highlighter-rouge">make</code> and <code class="highlighter-rouge">make test</code> successfully build</p>
  </li>
  <li>
    <p><code class="highlighter-rouge">make test</code> also successfully builds with the unchanged version of the test program in the starter</p>
  </li>
</ul>

<h2 id="grading">Grading</h2>

<p>To grade this assignment, we will:</p>

<ul>
  <li>Verify that your submission builds correctly, with no warnings. Warnings and/or build errors result in automatic deductions. Clean build always!</li>
  <li>Run automated tests that exercise the functionality of your <code class="highlighter-rouge">backtrace.c</code> and <code class="highlighter-rouge">malloc.c</code> modules. These tests will touch on all required features of the module.</li>
  <li>Go over the tests you added to <code class="highlighter-rouge">tests/test_backtrace_malloc.c</code> and evaluate them for thoughtfulness and completeness in coverage.</li>
  <li>Review your code and provide feedback on your design and style choices.</li>
</ul>

<h2 id="heap-alligators">Heap alligators</h2>
<p>At home, my chatter about “heap allocators” was mistaken for “heap alligators” by my kids, who were alarmed that I would ask my students to wrestle with such fearsome beasts.</p>

<p>A family trip to Gatorland taught us that a heap <strong>of</strong> alligators can actually be quite cute (see photo) and the adorable drawing from Jane Lange (talented CS107e alum and undergrad TA) demonstrates that a tamed heap alligator makes a lovely pet. May you, too, tame your heap alligator and triumph over the perils of dynamic allocation.</p>

<p><a href="images/heap_of_alligators.jpg"><img title="Heap of Alligators" src="images/heap_of_alligators.jpg" width="50%" style="display:inline;" /></a><a href="images/heapalligator.png"><img title="Heap Alligator" src="images/heapalligator.png" width="50%" style="float:right;" /></a></p>


  </div>
  <div class="toc-column col-lg-2 col-md-2 col-sm-2 hidden-xs">
    <div id="toc" class="toc" data-spy="affix" data-offset-top="0"></div>
  </div> 
</div>

  <script src="/_assets/tocbot.min.js"></script>
  <link rel="stylesheet" href="/_assets/tocbot.css">

  <script>
    tocbot.init({
      // Where to render the table of contents.
      tocSelector: '#toc',
      // Where to grab the headings to build the table of contents.
      contentSelector: '#main_for_toc',
      // Which headings to grab inside of the contentSelector element.
      headingSelector: 'h2, h3, h4',
    });
  </script>



  </div>
  <div class="footer navbar-default navbar-static-bottom">
    <p style="font-size: 65%; color:green; text-align:center;">
      <i>CS107e Winter 2020 &middot; Site generated 2020-04-19 20:35</i>
  </p>
</div> 
</body>

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</html>