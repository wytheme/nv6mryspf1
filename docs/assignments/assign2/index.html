<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/_assets/site.css" rel="stylesheet">
    <link href="/_assets/favicon.png" rel="icon" type="image/png">
    <title>CS107E Assignment 2: Implement a Clock</title>
  </head>

<body>
  <!-- Include this HTML partial to set up navbar -->

<nav class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
    <!-- hamburger -->
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>

  <img class="navbar-brand navbar-brand-image" src="/_assets/berry.png" >
  <a class="navbar-brand" href="/">CS107e Winter 2020</a>
  </div>

  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/">Home</a></li>
      <li><a href="/assignments">Assignments</a></li>
      <li><a href="/labs">Labs</a></li>
      <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Resources
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/about/">About the course</a></li>
            <li><a href="/policies/">Policies</a></li>
            <li><a href="/schedule/">Schedule</a></li>
            <li><a href="/guides/">Guides</a></li>
            <li><a href="/project_gallery/">Project gallery</a></li>
            <li><a href="/demos/">External demos</a></li>
            <li><a href="/resources/">External resources</a></li>
          </ul>
        </li>
      <li><a href="https://github.com/cs107e/cs107e.github.io">Repository</a></li>
    </ul>
  </div>
</nav>


  <div class="container" style="max-width:55em;">
    <h1 class="title">Assignment 2: Implement a Clock</h1><hr>
    
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" id="main_for_toc">
    
<p><em>Written by Pat Hanrahan; updated by Julie Zelenski</em></p>

<!-- If duedate set in front_matter, use that, otherwise calculate based on n*weeks + assign1 due -->

<p><b>Due: Tuesday, January 28 at  6:00 pm</b></p>

<h3 id="goals">Goals</h3>
<p>Your next assignment is to build a clock using a 4-digit 7-segment
display.</p>

<p>The goals of this assignment are for you to:</p>

<ul>
  <li>Get started with bare metal programming in C on the Raspberry Pi.</li>
  <li>Begin building a simple, modular library of useful functions.</li>
  <li>Learn how to write and use automated test cases to validate your code’s behavior.</li>
  <li>Learn how to use the Raspberry Pi’s system timer peripheral.</li>
  <li>Use your gpio and timer modules in the clock application.</li>
</ul>

<h3 id="get-starter-files">Get starter files</h3>
<p>Change to the <code class="highlighter-rouge">cs107e.github.io</code> repository in your <code class="highlighter-rouge">cs107e_home</code> and do a <code class="highlighter-rouge">git pull</code> to ensure your courseware files are up to date.</p>

<p>To get the assignment starter code, change to your local assignments repository, fetch changes from the remote ,and switch to the assignment basic branch:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ cd ~/cs107e_home/assignments
$ git fetch origin
$ git checkout assign2-basic
$ ls
Makefile apps     cstart.c gpio.h   start.s  timer.c
abort.c  assert.h gpio.c   memmap   tests    timer.h
</code></pre></div></div>

<p>Skim through each of the provided files to learn the overall structure of the project and review the skeletal starter code. The project includes a few <a href="#magic">magic files</a> (<code class="highlighter-rouge">memmap</code>, <code class="highlighter-rouge">start.s</code>, and <code class="highlighter-rouge">cstart.c</code>) that are necessary for bare metal C programs. We will explain their purpose in the upcoming lectures.</p>

<h3 id="modular-design">Modular design</h3>

<p>For this assignment, you will implement functions
across three different C files: <code class="highlighter-rouge">gpio.c</code>, <code class="highlighter-rouge">timer.c</code>, and <code class="highlighter-rouge">apps/clock.c</code>.  The first two files implement reusable modules, the latter is the application program file. <strong>Do not create any new source files or directories; add your code by editing the given files.</strong></p>

<p>The <code class="highlighter-rouge">gpio</code> and <code class="highlighter-rouge">timer</code> modules provide access to two of the Raspberry Pi peripherals. The <code class="highlighter-rouge">gpio</code> module has routines that control the GPIO pins and the <code class="highlighter-rouge">timer</code> module retrieves the system tick count. The two modules are used by the clock application, but, more broadly, these modules are designed to be reusable in any future application that requires similar functionality. These two modules are the first of many more to come. By the end of the quarter, you will have implemented a complete set of modules that you can package into a library of core functions for the Raspberry Pi.</p>

<p>A module is divided into an <em>interface</em> and its <em>implementation</em>. The module interface is described in its header file, e.g. <code class="highlighter-rouge">gpio.h</code> or <code class="highlighter-rouge">timer.h</code>. Each public function is listed with its name, prototype, and documentation about what the function does. The module implementation is in the corresponding <code class="highlighter-rouge">.c</code> file where those functions are defined.</p>

<p>For the <code class="highlighter-rouge">gpio</code> and <code class="highlighter-rouge">timer</code> modules, we specify the module interface; it is your job to write the module implementation to match it. You are not to change anything in the <code class="highlighter-rouge">gpio.h</code> and <code class="highlighter-rouge">timer.h</code> header files. All your edits will be in the <code class="highlighter-rouge">gpio.c</code> and <code class="highlighter-rouge">timer.c</code> source files. You should not export additional public <code class="highlighter-rouge">gpio_</code> or <code class="highlighter-rouge">timer_</code> functions. However, you can add your own private helper functions by declaring those functions at the top of the C file with the <code class="highlighter-rouge">static</code> keyword to make them only accessible to the implementation.</p>

<p>The file <code class="highlighter-rouge">apps/clock.c</code> contains your application program code that configures and operates your clock. The clock application does not directly access the peripherals, instead it calls on the public functions of the <code class="highlighter-rouge">gpio</code> and <code class="highlighter-rouge">timer</code> modules. There is no required interface for the clock application, but you should strive for a clean and well-decomposed program that would make your CS106 section leader proud.</p>

<p>The subsequent assignments in CS107e will be structured similarly. Each week you will implement new modules to add your Raspberry Pi library in the context of an application program that uses those modules.</p>

<h3 id="testing">Testing</h3>

<p>Now that you’re writing larger programs, we want to introduce you to strategies for how to test your program. We
hope these ideas will serve you well throughout your programming life.</p>

<p>The starter project includes an additional program file <code class="highlighter-rouge">tests/test_gpio_timer.c</code> that
defines an alternate <code class="highlighter-rouge">main()</code> function. Instead of running the clock, this <code class="highlighter-rouge">main()</code> function makes a series of calls to <code class="highlighter-rouge">gpio</code> and <code class="highlighter-rouge">timer</code> functions and uses <code class="highlighter-rouge">assert()</code> in order to validate the operations work correctly. Use <code class="highlighter-rouge">make test</code> to build and run this test program.</p>

<p>Recall from Lab 2 how <code class="highlighter-rouge">assert()</code> drives the red and green LEDs
on the Pi as a simple status indicator. If an assert fails, the program
halts and blinks the red LED of doom. If all asserts pass and
the program finishes normally and the green LED of happiness will turn on. Your goal is to make that little green light shine!</p>

<p>The given <code class="highlighter-rouge">tests/test_gpio_timer.c</code> only has a few very simple tests. You should extend the test program
with many additional tests of your own so as to thoroughly exercise the
functionality of your modules. <em>Unit-testing</em> each module in isolation before
going on to integrate its use into an application is an important strategy for
managing the development process as your programs become larger and more complex.</p>

<p>Learning how to effectively test your code is an important component of growing your programming skills and we want to be sure to reward for your testing efforts. The breadth and depth of the tests you add
to <code class="highlighter-rouge">tests/test_gpio_timer.c</code> will be used to determine a portion of your grade for this assignment (that is,
to get full credit on this assignment, you will need to include a thorough
suite of tests as part of your submission).</p>

<h3 id="basic-part-a-simple-clock">Basic part: a simple clock</h3>
<p><a name="clock_spec"></a></p>
<h4 id="1-wire-up-clock-hardware">1. Wire up clock hardware</h4>

<p>The first step is to build the hardware for your display.</p>

<ul>
  <li>Complete the breadboard circuit for the 4-digit 7-segment display you started in <a href="/labs/lab2">Lab 2</a>. Test your breadboard with jumper cables so that you know the wiring is correct before you connect it to the Raspberry Pi.</li>
  <li>In total, you will use 13 GPIOs on the Pi: eight pins to control which segments are on, four to control which digits are on, and one to read the state of the start button.
    <ul>
      <li>Connect GPIO pins 20-26 to the 1K current-limiting
resistors on your breadboard that drive segments A through G. GPIO 20 controls segment A, GPIO 21 controls segment B, and so on. An 8th pin, GPIO 27, can be used to control the decimal point, which is optional.</li>
      <li>Connect GPIO pins 10-13 to the base of the transistors controlling digits
1 through 4. GPIO 10 controls the first digit, GPIO 11 the second digit, and so on.</li>
      <li>Connect GPIO 2 to the red start button. The start button should be connected to the power rail through a 10K pull-up resistor; making the default state high. Pressing the button grounds the circuit, pulling the reading low.</li>
    </ul>
  </li>
  <li>At this point if you output 3.3V on GPIO 20 and 3.3V on GPIO 10, you should turn on segment A of digit 1. Hooray!</li>
  <li>Snap a photo of your finished hardware setup, add the photo to your repo with the name <code class="highlighter-rouge">board</code>, and commit the image file to your repo. We want to see your beautiful handiwork!</li>
</ul>

<p><img src="images/clock_connected.jpg" alt="clock connected" width="80%" />
Here’s a photo of our clock breadboard connected to the Raspberry Pi. We selected jumper colors in a repeating pattern (yellow-green-blue-violet) to help identify which connection is which.</p>

<h4 id="2-implement-and-test-gpio-module">2. Implement and test gpio module</h4>

<p>Review Chapter 6 of the <a href="/readings/BCM2835-ARM-Peripherals.pdf#page=89">Broadcom BCM2835 peripheral manual</a> which documents the GPIO peripheral. The device registers <code class="highlighter-rouge">FSEL</code>, <code class="highlighter-rouge">SET</code>, <code class="highlighter-rouge">CLR</code> and <code class="highlighter-rouge">LEV</code> will be used in your <code class="highlighter-rouge">gpio</code> module implementation.</p>

<p>Open the file <code class="highlighter-rouge">gpio.h</code> in your project directory and read the module documentation to learn about the module as a whole and the expected behavior of each function. Your functions must correctly implement each of the public functions. In addition, your functions should also be robust against client error. If asked to set an invalid pin or function, your function should not blunder on to wrong or broken behavior. The function documentation in the module interface gives specific guidance on the expected handling for improper calls.</p>

<p>We recommend that the first operations you implement are <code class="highlighter-rouge">gpio_set_function</code> and <code class="highlighter-rouge">gpio_get_function</code>. These operations use the <code class="highlighter-rouge">FSEL</code> device registers to configure the mode of a GPIO pin. Thus far we have only seen the input and output modes, see the module interface for the additional alternate modes that you must support. Implement <code class="highlighter-rouge">gpio_set_input</code> and <code class="highlighter-rouge">gpio_set_output</code> as simple wrapper functions that call <code class="highlighter-rouge">gpio_set_function</code> to input or output mode.</p>

<p>Before moving on, test what you’ve implemented. Edit the <code class="highlighter-rouge">main()</code> function of <code class="highlighter-rouge">tests/test_gpio_timer.c</code> to uncomment the call to <code class="highlighter-rouge">test_gpio_set_get_function()</code>.  Run <code class="highlighter-rouge">make test</code> to build and execute the test program on the Pi. If the green LED on the Pi turns on, all tests ran
successfully – bravo! If instead the Pi’s red LED is flashing, this means
you are failing a test.  If neither LED lights up, your program may be freezing or
crashing somewhere during the tests.</p>

<p>If you don’t get the green light, comment out all of the asserts within   <code class="highlighter-rouge">test_gpio_set_get_function()</code>. Uncomment the asserts one-by-one and re-run after each addition to determine which specific test is
failing. Use that information to narrow in on your bug, fix it,
and re-test until you achieve the green light of nirvana.</p>

<p>Passing these tests shows you’re off to a great start, but the provided test coverage is not comprehensive. You will need to add test cases of your own to definitively confirm the full correctness of your module. To jumpstart your brainstorming, consider what additional tests could be used to:</p>

<ul>
  <li>confirm setting a GPIO pin to alternate function modes, not just input or output</li>
  <li>confirm can configure a GPIO pin in each of the different FSEL registers</li>
  <li>confirm no interference when configuring multiple GPIO pins within the same FSEL register</li>
  <li>confirm that improper requests are gracefully handled</li>
</ul>

<p>What other cases can you think of to add to the above? As you add each new test case, build and re-run your test program to verify success. Don’t move on until you are bathed in green light on your now-comprehensive test suite.</p>

<!-- This partial will open warning/danger callout box  -->

<div style="background-color:#ffcccc; color:#993333; border-left: 5px solid #993333;margin: 5px 25px; padding: 5px;">

  <p>When writing test cases, take note that certain GPIO pins exhibit
specialized behavior. GPIO pins 14 and 15 are used for serial
transmission and by default have the function <code class="highlighter-rouge">GPIO_FUNC_ALT5</code>.  Manipulating these pins will disrupt the communication between your computer and the Pi. <strong>You do not need to include pins 14 and 15 in your tests.</strong> GPIO 35 controls the red power LED on
the Pi and GPIO 47 the green ACT LED. If you manipulate these pins, the corresponding LED will be affected. This is to be expected.</p>
</div>

<p>The remaining gpio functions to implement are <code class="highlighter-rouge">gpio_write</code> and <code class="highlighter-rouge">gpio_read</code>. These functions read and write the current pin state by accessing the <code class="highlighter-rouge">SET</code>, <code class="highlighter-rouge">CLR</code>, and <code class="highlighter-rouge">LEV</code> device registers.</p>

<p>After you’ve implemented these functions, test them by uncommenting the call to
<code class="highlighter-rouge">test_gpio_read_write()</code> in the <code class="highlighter-rouge">main()</code> function of <code class="highlighter-rouge">tests/test_gpio_timer.c</code>. Re-run the test program, look for the green light, and make necessary fixes until you achieve success.</p>

<p>Remember that the test coverage provided in the starter code is a good start, but you will need to supplement with additional test cases to achieve thorough coverage.  The test cases used by the automated tester in grading are quite rigorous. Rather than wait for our grading tests to ferret out issues for you to fix, the challenge goal for you is to find and fix any problems before you submit. We want your code to sail through grading with flying colors.</p>

<p>Keep up the habit of <strong>frequent git commits</strong> and push to sync history with your GitHub remote repository. This gives you an audit trail of your progress and an “off-site” backup to ensure that all of your great work is safely recorded.</p>

<p><strong>A note on volatile</strong></p>

<ul>
  <li>
    <p>Writing code that correctly interacts with a peripheral will require understanding of the
<code class="highlighter-rouge">volatile</code> keyword.  Remember from lecture that qualifying a variable as <code class="highlighter-rouge">volatile</code> indicates that its value could change
unexpectedly. Consider this pointer to the memory-mapped GPIO device
register <code class="highlighter-rouge">LEV0</code>:</p>

    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>  unsigned int *lev = (unsigned int *)0x20200034;
</code></pre></div>    </div>

    <p>The data at that address can, and should, be qualified as <code class="highlighter-rouge">volatile</code>:</p>

    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>  volatile unsigned int *lev = (unsigned int *)0x20200034;
</code></pre></div>    </div>

    <p>Adding the <code class="highlighter-rouge">volatile</code> qualifier informs the compiler that the <code class="highlighter-rouge">unsigned int</code> value can change due to events not apparent in the code. You read the LEV0 register to get the current value of an input pin. Whether that value read is 0 or 1 depends on what is physically connected to the pin, which can change externally. If every iteration of a loop in C reads from <code class="highlighter-rouge">*lev</code> and there is no intervening write to that location inside the loop, the optimizer could move the read operation outside the loop and only execute it once, on the assumption that the value will not change. If <code class="highlighter-rouge">lev</code> is qualified as pointing to a <code class="highlighter-rouge">volatile</code> int, the compiler cannot make that assumption and must re-read the value for each and every access.</p>

    <p>Note that <code class="highlighter-rouge">volatile</code> is not something to throw about willy-nilly. Apply it thoughtfully and intentionally to those specific data values that need to be treated in this special manner. Extraneous use of <code class="highlighter-rouge">volatile</code> can be misleading and will reduce performance as it disallows the compiler from making optimizations.</p>
  </li>
</ul>

<p>Congratulations on completing your first Raspberry Pi module!</p>

<h4 id="3-display-a-digit">3. Display a digit</h4>

<p>For your next task, turn your attention to the clock application and put your shiny new gpio module to work to drive the 7-segment display.</p>

<p>In <code class="highlighter-rouge">apps/clock.c</code>, create an array of 16 elements, one for each
hexadecimal value between 0 and 15. Each array element should
be a byte (8-bits). Bit 0 (the least significant) will represent
segment A, bit 1 segment B, and so on. If a bit is set, then that
segment should be lit. For example, digit <code class="highlighter-rouge">0</code> consists of segments A-F, so its bit pattern is <code class="highlighter-rouge">0b00111111</code>. Digit <code class="highlighter-rouge">1</code> consists of just segments B and C, so its bit pattern is <code class="highlighter-rouge">0b00000110</code>.  (<code class="highlighter-rouge">0b</code> is the prefix that introduces a binary number
literal, just as <code class="highlighter-rouge">0x</code> prefixes a hexadecimal literal.)</p>

<p>Bit 7 (the most significant) will be used to represent <code class="highlighter-rouge">DP</code>, the dot. Since we won’t be
using the dot in this assignment, bit 7 should always be 0.</p>

<p>Create bit patterns for all the digits 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
A, B, C, D, E, F. You won’t be displaying A-F for this assignment, but they
may be useful in the future.  This <a href="http://www.uize.com/examples/seven-segment-display.html">interactive demo of a 7-segment display </a>
is handy for seeing how the bit pattern relates to the character segments.</p>

<p>Add code to the <code class="highlighter-rouge">main()</code> function of <code class="highlighter-rouge">apps/clock.c</code> to display a single digit. Verify that your bit patterns are correct by displaying each digit value from <code class="highlighter-rouge">0</code> to <code class="highlighter-rouge">F</code>.  Use <code class="highlighter-rouge">make install</code> to build and run your clock application.</p>

<h4 id="4-implement-and-test-timer-module">4. Implement and test timer module</h4>

<p>In order to implement a clock, we’ll need some way to determine the passage 
of time. Thankfully, the Raspberry Pi includes a “system timer”. The system timer is an on-board peripheral that is initialized to zero when the Pi powers up and is 
continuously incremented once every microsecond behind the scenes.</p>

<p>Chapter 12 of the
<a href="/readings/BCM2835-ARM-Peripherals.pdf#page=172">Broadcom BCM2835 Peripherals Manual</a>
contains the documentation for the system timer peripheral. For this module, note that we only care about the lower
32-bits of the system timer. Don’t forget that we use
<a href="/readings/BCM2835-ARM-Peripherals.pdf#page=6">ARM physical addresses</a>,
not bus addresses (0x7E… for peripherals), so you’ll need to
change the 0x7E… prefix in any peripheral address to 0x20.</p>

<p>The <code class="highlighter-rouge">timer</code> module defines functions that access the system timer. Read the interface to the module in the <code class="highlighter-rouge">timer.h</code> header. Review the given code for the module implementation in <code class="highlighter-rouge">timer.c</code>.</p>

<p>Your one task for the <code class="highlighter-rouge">timer</code> module is to implement the function <code class="highlighter-rouge">timer_get_ticks</code> to read
the current system tick count from the system timer.</p>

<p>Uncomment the call <code class="highlighter-rouge">test_timer()</code> in <code class="highlighter-rouge">tests/test_gpio_timer.c</code>. Use <code class="highlighter-rouge">make test</code> to build and re-run the test program. Verify the given tests succeed and then consider what additional tests are needed for the timer module (there may not be much; it is a pretty simple module). Once both the gpio and timer modules are passing all of your tests, you’re ready to tackle the rest of the clock application.</p>

<h4 id="5-write-display-refresh-loop">5. Write display refresh loop</h4>

<p>GPIO pins 20 to 26 drive the seven segments A to G. Segments A to G are shared by all four digits on the display. There is no way to turn on the display segments to show a <code class="highlighter-rouge">5</code> on the leftmost digit while simultaneously showing a <code class="highlighter-rouge">3</code> on the rightmost digit.</p>

<p>Instead, you will write a display refresh loop that drives the four digits by iterating over the
digits one-by-one in quick succession. It turns on the segments for the leftmost digit, waits a short
length of time, and turns off that digit, then repeats the process for each of the other three digits. You might think that turning a
digit on and off would cause it to flicker. The key is to sequence through
the digits so fast that our eyes cannot see them changing. Good thing
computers are fast!</p>

<p>Implement the display refresh loop in <code class="highlighter-rouge">apps/clock.c</code>. Use the functions from the <code class="highlighter-rouge">timer</code> module to control the wait time. Loop though all
four digits, turning each on for 2500 microseconds. Do you see any flicker?</p>

<h4 id="6-implement-clock">6. Implement clock</h4>

<p>The program for the basic assignment starts by showing <code class="highlighter-rouge">----</code> on the display and waits for the user to click the start button. The clock then displays the elapsed minutes and seconds since start. A loop continually refreshes the display, updating the value displayed to match the elapsed time. Make sure to test that the timer is calibrated correctly
so the clock is counting at the right rate.</p>

<p>The clock time is counting elapsed time since the clock was started, which is not quite the value as the system tick count.  Clock time and the system tick count change at the same rate, but start at different values. Your program will need to implement this logic.</p>

<p>This video shows starting our Raspberry Pi clock and counting to 12 seconds:</p>

<iframe width="420" height="236" src="https://www.youtube-nocookie.com/embed/PaYcgyyuA60?modestbranding=1&amp;version=3&amp;loop=1&amp;playlist=PaYcgyyuA60" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>

<p><strong>Mission accomplished!</strong> Follow our <a href="#submit">submit instructions</a> and it’s time to celebrate. You have wired up a complex breadboard circuit, written two reusable library modules and a clock application, as well as working up a methodology for testing your code. Be sure to show off your spiffy new clock to your followers (but probably not a good idea to try to get through a TSA screening with it in your bag…).</p>

<h3 id="extension-set-time">Extension: Set time</h3>
<p>If you’re looking for an extra challenge, consider doing the optional extension to create an interface to set the clock time.</p>

<p>After you have submitted your basic version, create an extension branch with</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ git checkout -b assign2-extension
</code></pre></div></div>

<p>Add a second blue button to your clock breadboard next to the red button. Connect the blue button to GPIO pin 3.
Design a user interface that allows you to set the time using these buttons. You should be able to
set the minutes and seconds separately.  Strive for an interface design that is
easy to use and that works even after the clock starts running. It can be
challenging to build an <a href="https://dilbert.com/strip/2016-06-12">interface with just a few buttons</a>!</p>

<p>Commit a <code class="highlighter-rouge">README.md</code> file on your <code class="highlighter-rouge">assign2-extension</code> branch that describes your extension. Your documentation should explain to the user how to set the time via the red and blue buttons.</p>

<p>When reading button presses on the Pi, you will quickly realize that pressing
the button once may cause the value on the GPIO pin to change multiple times.
This is due to physical characteristics of the button mechanism which cause the
button circuit to open and close multiple times during a press.  To address
this issue, implement <strong>debouncing</strong> by checking whether the value change on
the GPIO pin corresponds to an actual button press or one of these
spurious events.  This can be done by checking that the GPIO pin reads the
button press value for a long enough time (that is, these spurious events will
change the GPIO value quickly, meaning if you check the value of the pin as
pressed, then wait a bit longer and see it as unpressed, that means it was a
spurious event). Experiment with the amount of time required to register a
valid press so that your buttons click easily without having to hold them down
too long, but do not generate extra button press events for a single physical
button press.</p>

<h3 id="magic-files-">Magic files <a name="magic"></a></h3>

<p>The starter project contains additional code files <code class="highlighter-rouge">start.s</code> and <code class="highlighter-rouge">cstart.c</code> and the file <code class="highlighter-rouge">memmap</code>. The <code class="highlighter-rouge">memmap</code> file is used by the linker when building the program and <code class="highlighter-rouge">start.s</code> and <code class="highlighter-rouge">cstart.c</code> provide the minimum startup to set up a C program to run in the bare metal environment. A bit later in the course, we will study these files to understand how they work and why they are necessary. Feel free to look into the files now to get a sneak preview, but don’t worry if you don’t fully understand them yet. We will discuss them in more detail soon!</p>

<h3 id="submit">Submit</h3>
<p>The deliverables for <code class="highlighter-rouge">assign2-basic</code> are:</p>

<ul>
  <li>a photo of your completed breadboard</li>
  <li>implementation of the <code class="highlighter-rouge">gpio.c</code> and <code class="highlighter-rouge">timer.c</code> modules</li>
  <li>your comprehensive tests for both modules in <code class="highlighter-rouge">tests/test_gpio_timer.c</code></li>
  <li>application program <code class="highlighter-rouge">apps/clock.c</code></li>
</ul>

<p>Submit the finished version of your assignment by making a git “pull request”,
following the steps given in the <a href="/assignments/assign0/">Assignment 0 writeup</a>.
Remember to make separate pull requests for your basic and extension
submissions.</p>

<h3 id="grading">Grading</h3>

<p>To grade this assignment, we will:</p>

<ul>
  <li>Verify that your project builds correctly, with no warnings (see Note 1 below)</li>
  <li>Run automated tests that exercise the functionality of your <code class="highlighter-rouge">gpio.c</code> and <code class="highlighter-rouge">timer.c</code> modules. These tests will touch on all required features of the module. (see Note 2 below)</li>
  <li>Observe your clock application running on a Raspberry Pi wired to a clock breadboard and confirm it operates correctly. Our hardware setup will be configured exactly as specified in <a href="#clock_spec">Step 1 above</a>.</li>
  <li>Admire the photo you submit of your completed breadboard.</li>
  <li>Go over the tests you added to <code class="highlighter-rouge">tests/test_gpio_timer.c</code> and evaluate them for thoughtfulness and completeness in coverage.</li>
  <li>Review your code and provide feedback on your design and style choices.</li>
</ul>

<p>Note 1: The automated tester will <strong>deduct half a point for any warnings generated
when compiling your code</strong>. Warnings are the way the compiler draws attention to a code passage that isn’t an outright error but appears suspect. Some warnings are mild/harmless, but others are critically important. If you get in the habit of keeping your code compiling cleanly, you’ll never miss a crucial message in a sea of warnings you are casually ignoring. You may want to consider adding the flag <code class="highlighter-rouge">-Werror</code> to CFLAGS in your Makefile.
This converts all warnings into hard errors, which block the build and guarantees no warnings will be overlooked and left unresolved.</p>

<p>Note 2: If your modules have failures on the automated tests, we will report those to you by filing issues.  To promote the practice of fixing your bugs, you will have the opportunity to re-submit with corrections for these issues for a
partial refund of the points originally deducted.
Furthermore, fixing bugs in your modules now will put you in a better place to earn
the full system bonus at the end of the quarter. The system bonus points are awarded for submitting a working system using entirely your own code written for the assignments.</p>


  </div>
  <div class="toc-column col-lg-2 col-md-2 col-sm-2 hidden-xs">
    <div id="toc" class="toc" data-spy="affix" data-offset-top="0"></div>
  </div> 
</div>

  <script src="/_assets/tocbot.min.js"></script>
  <link rel="stylesheet" href="/_assets/tocbot.css">

  <script>
    tocbot.init({
      // Where to render the table of contents.
      tocSelector: '#toc',
      // Where to grab the headings to build the table of contents.
      contentSelector: '#main_for_toc',
      // Which headings to grab inside of the contentSelector element.
      headingSelector: 'h2, h3, h4',
    });
  </script>



  </div>
  <div class="footer navbar-default navbar-static-bottom">
    <p style="font-size: 65%; color:green; text-align:center;">
      <i>CS107e Winter 2020 &middot; Site generated 2020-04-19 20:35</i>
  </p>
</div> 
</body>

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</html>